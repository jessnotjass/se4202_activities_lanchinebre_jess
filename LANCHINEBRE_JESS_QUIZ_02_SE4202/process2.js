const ipc = require('node-ipc')

ipc.config.id = 'process2'
ipc.config.retry = 1500
ipc.config.silent = true
ipc.connectTo('process1', () => {
  ipc.of.process1.on('connect', () => {
    ipc.of.process1.emit('GET')
    console.log('get_data')
    ipc.of.process1.emit('INSERT', {
      item: '1'
    })
    console.log('insert_data')
    ipc.of.process1.emit('GET')
    console.log('get_data')
  })
})
