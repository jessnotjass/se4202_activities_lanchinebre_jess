
const ipc = require('node-ipc')

ipc.config.id = 'process1'
ipc.config.retry = 1500
ipc.config.silent = true

const collection = []

ipc.serve(() => ipc.server.on('GET', () => {
  try {
    console.log(collection)
  } catch (e) {
    console.log(e)
  }
}))

ipc.serve(() => ipc.server.on('INSERT', (data) => {
  try {
    collection.push(data)
    console.log(data)
  } catch (e) {
    console.log(e)
  }
}))

ipc.server.start()
console.log('ipc server started')
